
let tl, w, h, titleStyle, sprite_holder;
let data, app, title_container, imgs;
let counter = 0;
let title_size = 40; // Scales for smaller screen sizes.

const LETTERBOX = true; // Configure how images are resized.
const UNFURL_DOWN = true; // Configure whether title box stretches up or down into view.

const WEATHER_HEIGHT = 180;
const WEATHER_PADDING = 20;
const TITLE_BOX_HEIGHT = 150;
const TITLE_BOX_MARGIN = 100;
const TITLE_BOX_PADDING = 100;
const TITLE_BOX_PADDING_BOTTOM = 100;
const SQUISH = 100 / TITLE_BOX_HEIGHT; // weird hack...
const WAIT_INTERVAL = 3000;
const STRIPE_WIDTH = 70;

let current_sprite, next_sprite;
let current_title, next_title;

const weather_img = 'https://ssl.gstatic.com/onebox/weather/64/partly_cloudy.png';

const completionNext = () => {
    title_container.removeChild(current_title);
    sprite_holder.removeChild(current_sprite);
    // Def need these lines:
    current_sprite = next_sprite;
    current_title = next_title;

    const idx = (counter + 1) % data.Content.length; // Counter increments in `next`.

    // New title:
    const text = data.Content[idx].Title;
    next_title = new PIXI.Text(text, titleStyle);
    next_title.alpha = 0;
    next_title.x = TITLE_BOX_PADDING;
    const text_height = positionText(text, titleStyle);
    next_title.y = SQUISH * ((TITLE_BOX_HEIGHT - text_height) / 2);
    next_title.height *= SQUISH; // Total hack -- no idea why needed...

    // New sprite:
    next_sprite = new PIXI.Sprite(PIXI.loader.resources[imgs[idx]].texture);
    resize(next_sprite);
    next_sprite.alpha = 0;
    next_sprite.anchor.set(0.5);
    next_sprite.x = w / 2;
    next_sprite.y = h / 2;

    title_container.addChild(next_title);
    sprite_holder.addChild(next_sprite);

    next();
}

const next = () => {
    counter++;

    setTimeout(() => {
        tl.to(current_title, 1.3, { alpha: 0 })
            .to(current_sprite, 1.6, { alpha: 0 }, "-=1.1") // Ugly way of doing this...
            .to(next_title, 1.3, { alpha: 1 }, "-=0.9")
            .to(next_sprite, 1.6, { alpha: 1, onComplete: completionNext }, "-=1.55") // Cross fade!
    }, WAIT_INTERVAL);
}

const appendText = () => {
    titleStyle = new PIXI.TextStyle({ fill: 0xffffff, fontSize: title_size, fontFamily: 'Helvetica', wordWrap: true, wordWrapWidth: w - 200 });
    const text = data.Content[counter].Title; // Make sure counter is a single source of truth.
    current_title = new PIXI.Text(text, titleStyle);
    current_title.alpha = 0;
    current_title.x = TITLE_BOX_PADDING;

    // Center title text vertically:
    const text_height = positionText(text, titleStyle);
    current_title.y = SQUISH * (TITLE_BOX_HEIGHT - text_height) / 2; // Need SQUISH here too...

    current_title.height *= SQUISH; // Total hack -- no idea why needed....
    // Started needing it when we changed title_container's ending height from 100 to 150.... 
    // Seems so unlikely that 100px would just happen to be 100%. But where else would it be getting 100px as its base...?
    title_container.addChild(current_title);

    tl.to(current_title, 1, { alpha: 1 });

    // Stage next title:
    const next_text = data.Content[counter + 1].Title;
    next_title = new PIXI.Text(next_text, titleStyle);
    next_title.alpha = 0;
    next_title.x = TITLE_BOX_PADDING;
    const next_text_height = positionText(next_text, titleStyle);
    next_title.y = SQUISH * (TITLE_BOX_HEIGHT - next_text_height) / 2; // Need SQUISH here too...
    next_title.height *= SQUISH;
    title_container.addChild(next_title);

    // Call `next` for the first time to kick things off:
    next();
}

const animateNews = () => {
    title_container = new PIXI.Container();
    const title_box = new PIXI.Graphics();
    const title_box_stripe = new PIXI.Graphics();

    title_box
        .beginFill(0x66CCFF, 0.8)
        .drawRect(STRIPE_WIDTH, 0, w - TITLE_BOX_PADDING, 100) // Start with 0 height -- ON THE CONTAINER!
        .endFill();
    title_box_stripe
        .beginFill(0x079ed7, 0.8)
        .drawRect(0, 0, STRIPE_WIDTH, 100)
        .endFill();

    title_container.addChild(title_box);
    title_container.addChild(title_box_stripe);
    app.stage.addChild(title_container);

    title_container.y = UNFURL_DOWN ? h - TITLE_BOX_HEIGHT - TITLE_BOX_PADDING : h - TITLE_BOX_HEIGHT;
    title_container.x = TITLE_BOX_MARGIN;
    title_container.height = 0;

    tl.to(title_container, 1, { height: `+=${TITLE_BOX_HEIGHT}px`, onComplete: appendText });

    if (!UNFURL_DOWN) {
        tl.to(title_container, 1, { y: `-=${TITLE_BOX_HEIGHT}px` }, "-=1"); // Ugly hack but it works.
    }
}

const createGreeting = () => {
    const grt_container = new PIXI.Container();
    const grt = new PIXI.Text('News & Weather', { fill: 0xffffff, fontSize: 42 });

    grt_container.addChild(grt); // Need to do this *before* setting pivot, or will have 0 width/height.
    grt_container.pivot.x = grt_container.width / 2;
    grt_container.pivot.y = grt_container.height / 2;
    grt_container.x = w / 2;
    grt_container.y = h / 2; // This doesn't seem perfectly y-centered...

    grt_container.alpha = 0;
    app.stage.addChild(grt_container);

    tl.to(grt_container, 0.7, { alpha: 1 })
    tl.to(grt_container, 0.5, { alpha: 0 }, "+=1.5")
        .addLabel('introGone')

    animateNews();
}

// Feels like too much code for what it does....
const addWeather = () => {
    const wSprite = new PIXI.Sprite(PIXI.loader.resources[weather_img].texture);
    // Is there a way of doing this without having to stretch the image? Looks kind of grainy.
    wSprite.scale.x = 1.5;
    wSprite.scale.y = 1.5;
    wSprite.anchor.set(0.5);

    const wStyle = new PIXI.TextStyle({ fill: 0xffffff, fontSize: 55, fontFamily: 'Helvetica' })
    const wText = new PIXI.Text('44°', wStyle);

    const text_height = positionText('44°', wStyle);
    const text_width = getWidth('44°', wStyle);

    wText.pivot.y = text_height / 2;
    wText.y = WEATHER_HEIGHT / 2;
    wSprite.y = WEATHER_HEIGHT / 2;
    wSprite.x = wSprite.width / 2 + WEATHER_PADDING;
    wText.x = wSprite.x * 2;

    const wBox = new PIXI.Graphics();
    const box_width = 3 * WEATHER_PADDING + wSprite.width + text_width;
    wBox
        .beginFill(0x202020, 0.8)
        .drawRect(0, 0, box_width, WEATHER_HEIGHT) // Remember, this is relative to CONTAINER.
        .endFill();

    const wContainer = new PIXI.Container();
    wContainer.x = w - 220 - 3 * WEATHER_PADDING;
    wContainer.y = 100;
    wContainer.alpha = 0;

    wContainer.addChild(wBox);
    wContainer.addChild(wSprite);
    wContainer.addChild(wText);
    app.stage.addChild(wContainer);

    tl.to(wContainer, 1.8, { alpha: 1 }, "introGone");
}

// Load up the first image, fade it in after the intro fades out:
const setup = () => {
    // console.log(PIXI.loader.resources); // An object, not an array.... That's why sprites weren't rendering.
    current_sprite = new PIXI.Sprite(PIXI.loader.resources[imgs[0]].texture);
    resize(current_sprite);
    current_sprite.alpha = 0;
    // Center the image on the screen:
    current_sprite.anchor.set(0.5);
    current_sprite.x = w / 2;
    current_sprite.y = h / 2;
    sprite_holder.addChild(current_sprite);

    // Stage next sprite:
    next_sprite = new PIXI.Sprite(PIXI.loader.resources[imgs[1]].texture);
    resize(next_sprite);
    next_sprite.alpha = 0;
    next_sprite.anchor.set(0.5);
    next_sprite.x = w / 2;
    next_sprite.y = h / 2;
    sprite_holder.addChild(next_sprite);

    addWeather();

    tl.to(current_sprite, 1.8, { alpha: 1 }, "introGone");
}

const loadImages = () => {
    sprite_holder = new PIXI.Container();
    app.stage.addChild(sprite_holder); // must be first, so goes in the background, I imagine..

    imgs = data.Content.map(d => d.Media[0].Url);

    PIXI.loader
        .add(imgs)
        .add(weather_img)
        .load(setup);
}

const loadData = async () => {
    data = await $.get('https://kitchen.screenfeed.com/feed/q9zsfy2aqgstjkvy1wxk2541zw.json?formatter=nip&captions=false')
    loadImages();
    createGreeting();
}

$(document).ready(() => {
    w = window.innerWidth;
    h = window.innerHeight;
    tl = new TimelineMax();
    app = new PIXI.Application();

    document.body.appendChild(app.view);
    app.renderer.backgroundColor = 0x000000;
    app.renderer.resize(w, h);

    // Make font size semi-responsive:
    if (w < 500) {
        title_size = 20;
    } else if (w < 800) {
        title_size = 30;
    }
    if (w > 1800) title_size = 60;

    loadData();
});


// For centering title text vertically:
function positionText(text, style) {
    // Remember style needs to be a PIXI.TextStyle!
    const metrics = PIXI.TextMetrics.measureText(text, style);
    return metrics.height;
}

function getWidth(text, style) {
    const metrics = PIXI.TextMetrics.measureText(text, style);
    return metrics.width;
}

// For resizing images to fit screen:
function resize(sprite) {
    // Whichever is bigger is the direction we need to stretch (assuming no black stripes):
    const hor_diff = w - sprite.width;
    const vert_diff = h - sprite.height;
    // Note, we do *not* want to take the absolute value.

    const ratio = (hor_diff > vert_diff) ? w / sprite.width : h / sprite.height;
    const black_ratio = (hor_diff < vert_diff) ? w / sprite.width : h / sprite.height;

    if (LETTERBOX) {
        sprite.scale.x = black_ratio;
        sprite.scale.y = black_ratio;
    } else {
        sprite.scale.x = ratio;
        sprite.scale.y = ratio;
    }
}